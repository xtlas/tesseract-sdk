# OpenAPI\Client\SnowflakeApi

All URIs are relative to *https://virtserver.swaggerhub.com/quub/Snowflake/1.0.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**generateSnowflake**](SnowflakeApi.md#generateSnowflake) | **POST** / | Generates Unique snowflake.



## generateSnowflake

> \OpenAPI\Client\Model\InlineResponse200 generateSnowflake($snowflake)

Generates Unique snowflake.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new OpenAPI\Client\Api\SnowflakeApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$snowflake = new \OpenAPI\Client\Model\Snowflake(); // \OpenAPI\Client\Model\Snowflake | Pet object that needs to be added to the store

try {
    $result = $apiInstance->generateSnowflake($snowflake);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SnowflakeApi->generateSnowflake: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **snowflake** | [**\OpenAPI\Client\Model\Snowflake**](../Model/Snowflake.md)| Pet object that needs to be added to the store |

### Return type

[**\OpenAPI\Client\Model\InlineResponse200**](../Model/InlineResponse200.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json, application/xml
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

